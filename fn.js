// fn.js
// (C) 2013 KIM Taegyoon
// Functional JavaScript Library
// Licensed under the Apache License, Version 2.0

FN_VERSION="0.1";

// function shorthand: fn("a","a*2")
function fn(arg,body){
 return eval("(function("+arg+"){return "+body+"})");
}

// binary operator to function: bin("+")
function bin(op){
 return fn("a,b","a"+op+"b");
}

// range, inclusive
function range(start, end, step){
	var ret=[];
	if(step>=0){
	  for(var i=start; i<=end; i+=step){
		ret.push(i);
	  }
	}
	else{
	  for(var i=start; i>=end; i+=step){
		ret.push(i);
	  }
	}
	return ret;
}

function fold(f,arr){
 var r=arr[0];
 for(var i=1;i<arr.length;i++){
  r=f(r,arr[i]);
 }
 return r;
}

function map(f,arr){
 var r=[];
 for(var i=0;i<arr.length;i++){
  r.push(f(arr[i]));
 }
 return r;
}

function filter(f,arr){
 var r=[];
 for(var i=0;i<arr.length;i++){
  var c=f(arr[i]);
  if(c)r.push(arr[i]);
 }
 return r;
}
