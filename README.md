# fn.js: Functional JavaScript Library

(C) 2013 KIM Taegyoon

fn.js is a simple functional JavaScript library.

Try JavaScript here (after pasting fn.js): [http://tryjs.tistory.com/](http://tryjs.tistory.com/)

## Usage (in HTML) ##
```
<script src="fn.js"></script>
```

## File ##
* fn.js: The library

## Reference ##
### Functions
```
bin filter fn fold map range
```

### bin(op)

Creates a function from a binary operator **op**.
```
> bin("+")
function(a,b){return a+b}
```

### filter(f,arr)

Looks through each value in the array **arr**, returning an array of all the values that pass a truth test (**f**).
```
> filter(fn("a","a%2==0"),[1,2,3])
2
```

### fn(arg,body)

Function shorthand: (function(**arg**){return **body**}) **arg** and **body** is code in string form.
```
> fn("a,b","a+b")
function(a,b){return a+b}
```

### fold(f,arr)

Boils down a list of values into a single value. **arr**[0] is the initial state of the reduction, and each successive step of it should be returned by **f**.
```
> fold(bin("+"),range(1,10,1))
55
> fold(Math.pow,[2,3,4]) // (2^3)^4
4096
```

### map(f,arr)

Produces a new array of values by mapping each value in **arr** through a transformation function (**f**).
```
> map(fn("a","a*2"),[1,2,3])
2,4,6
```

### range(start,end,step)

Creates a list of numbers, inclusive.
```
> range(1,10,1)
1,2,3,4,5,6,7,8,9,10
```

## Examples ##

### [Project Euler Problem 1](http://projecteuler.net/problem=1) ###
```
fold(bin("+"),filter(fn("x","x%3==0||x%5==0"),range(1,999,1)))
```
=> 233168

## License ##

   Copyright 2013 KIM Taegyoon

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
